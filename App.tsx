import 'react-native-gesture-handler';
import {Provider} from "react-redux";
import {store} from "./services/store";
import React from "react";
import Router from "./navigators/Router";
import { NativeBaseProvider } from "native-base";
import ReportForm from './components/form/ReportForm';
import Ranking from './components/ranking/Ranking';
import { NativeRouter, Route, Routes } from "react-router-native";
import RegistrationForm from './screens/RegistrationScreen';


export default function App() {
    return (
        <Provider store={store}>
            <NativeRouter>
           
            <NativeBaseProvider>
           
            <Routes>
            <Route path='/' element={ <Router />  } />
         <Route path='/registration' element={<RegistrationForm />} />
         </Routes>
           </NativeBaseProvider>
           </NativeRouter>
        </Provider>
    );
}

