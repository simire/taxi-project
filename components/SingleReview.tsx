import {Text, View, Image, ImageSourcePropType, TouchableOpacity} from 'react-native'
import {styles} from './SingleRewiewStyle'
import {assets} from '../config'
import {ReviewModel} from '../types'
import {Feather, Ionicons} from "@expo/vector-icons";
import { useSelector } from 'react-redux';
import { selectAuth } from '../services/store/slices/authSlice';
import { useDeleteReviewMutation } from '../services/api/reviewApi';


interface Props {
    data: ReviewModel
}


const SingleReview = ({data}: Props) => {
    const [ deleteReview ] = useDeleteReviewMutation();
    const {user : currentUser} = useSelector(selectAuth);
    const filledStar = assets.star as unknown as ImageSourcePropType
    const user = assets.user as unknown as ImageSourcePropType;

    // this page was modified because the review's model required parameter did not include the ''fromUser'' propertie

    let firstname = '';
    let lastname = '';
    let profile = '';
    let id = 0;
    if (data.fromUser) {
        // let profile: string | null = data.fromUser.photoProfile
        if (profile !== null) {
            profile = profile.replace('/uploads/', '')
        }

        firstname = data.fromUser.firstname
        lastname = data.fromUser.lastname
        id = data.fromUser.id
    }

    const onClick = async () => {
        try {
            await deleteReview(data.id);
        } catch (e) {
            console.log(e.message)
        }
    }


    return (
        <View style={styles.container}>
            {currentUser.id === id && <TouchableOpacity style={styles.deleteButton} onPress={onClick}>
                <Feather name='trash-2' color='white' size={14} />
            </TouchableOpacity>}
            <View style={styles.reviewHeader}>
                <View>
                    {
                        profile ?
                            <Image
                                source={{uri: profile}}
                                style={styles.userImage}
                                resizeMode={"contain"}
                            />
                            :
                            <Image
                                source={user}
                                style={styles.userImage}
                                resizeMode={"contain"}
                            />
                    }
                </View>
                <View style={styles.userInfoAndReview}>
                    <View>
                        <Text style={styles.userName}>{firstname} {lastname}</Text>
                    </View>
                    <View style={styles.review}>
                        <View style={styles.stars}>
                            {
                                [...Array(data.star)].map((_, i) => (
                                    <Image source={filledStar} style={styles.star} key={i} />
                                ))
                            }
                        </View>
                        <Text style={styles.reviewScore}>{data.star}.0</Text>
                        <View style={styles.reviewDateContainer}>
                            <Text style={styles.reviewDate}>2 days ago</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.description}>
                <Text>{data.review}</Text>
            </View>
        </View>
    )
}

export {SingleReview}
