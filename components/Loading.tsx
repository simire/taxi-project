import React from 'react';
import {View, ActivityIndicator} from "react-native";
import {styles} from "./LoadingStyle";

const Loading = () => {
    return (
        <View style={styles.loading}>
            <ActivityIndicator color={'#000'} animating={true} size="small" />
        </View>
    );
};

export { Loading };