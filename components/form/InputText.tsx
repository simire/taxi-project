import React from 'react';
import {TextInput, TextInputProps, View} from "react-native";

const InputText: React.FC<TextInputProps&{container?: Object|null}> = ({
     container,
     style,
     onChangeText,
     placeholder,
     value,
    ...rest
}): JSX.Element => {
    return (
        <View style={container}>
            <TextInput
                style={style}
                value={value}
                placeholder={placeholder}
                onChangeText={onChangeText}
                {...rest}
            />
        </View>
    );
};

export default InputText;