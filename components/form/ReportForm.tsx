import React, { useState } from 'react';
import {ScrollView, Input, Stack, Button, Radio, Flex,  FormControl, TextArea, VStack, } from "native-base";
import { Text} from 'react-native';
import { useAddReportMutation, useGetReportsQuery } from '../../services/api/taxiApi';
import { onChange } from 'react-native-reanimated';

function ReportForm() {

    const [report, setReport] = useState({ name: '', title: '', message: ''})

    const [AddReport] = useAddReportMutation();
    const { data, error, isLoading, isSuccess } = useGetReportsQuery();

     console.log(report)
    const handleSubmit = async (e: any) => {
        e.preventDefault();
        AddReport(report)
        }
  return (
    <ScrollView>
      
    <Stack space={1} w="100%" maxW="360px" mx="auto" mt={12}>
    <Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginBottom: 10}}>REPORT</Text>
    <FormControl isRequired w="100%" minW="360px">
    <FormControl.Label>Full name</FormControl.Label>
    <Input size="xs" type='text' placeholder="Full name" value={report.name} onChangeText={(name: any) => setReport({...report, name})} />
    <FormControl.ErrorMessage>Full name is required</FormControl.ErrorMessage>
    </FormControl>
     <FormControl isRequired w="100%" minW="360px">
    <FormControl.Label>Message tile</FormControl.Label>
    <Input size="xs" type='text' placeholder="Message title" value={report.title} onChangeText={(title: any) => setReport({...report, title})} />
    <FormControl.ErrorMessage>Message title is required</FormControl.ErrorMessage>
    </FormControl>
    <FormControl isRequired w="100%" minW="360px">
    <FormControl.Label>message</FormControl.Label>
    <TextArea h={20} placeholder="message" w="100%" minW="360" value={report.message} onChangeText={(message: any) => setReport({...report, message})} />
    <FormControl.ErrorMessage>Message is required</FormControl.ErrorMessage>
    </FormControl>
   <Button style={{marginTop: 10}} onPress={handleSubmit}>Submit</Button>
   {isSuccess && data.map(result => 
   <VStack key={result.id}>
   <Text>{result.name}</Text>
   <Text>{result.title}</Text>
   <Text>{result.message}</Text>
   </VStack>
   )}
  </Stack>
  </ScrollView>

  )
}

export default ReportForm