import React from 'react';
import {View, ViewProps} from "react-native";
import { Link } from 'native-base';
import RegistrationForm from '../../screens/RegistrationScreen';
//import { Link } from "react-router-native";
import { useNavigate } from "react-router-native";

const LoginForm: React.FC<ViewProps> = ({style, ...props}) => {

    const navigate = useNavigate();
const handleRegister = () => {
    navigate('/registration')
}

    return (
        <View style={style} {...props}>
            {props.children}
            <View style={{flex: 1, alignItems: 'flex-start', marginTop: 10}}>
            <Link onPress={handleRegister}>Click to register</Link>
            </View>
        </View>
    );
};

export default LoginForm;