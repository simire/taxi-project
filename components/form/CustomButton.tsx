import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";

interface CustomButtonProps {
    onPress: () => void;
    text: string;
    style?: any;
    textStyle?: any;
}

const CustomButton:React.FC<CustomButtonProps> = ({onPress, text, style, textStyle}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={style}>
                <Text style={textStyle}>{text}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default CustomButton;