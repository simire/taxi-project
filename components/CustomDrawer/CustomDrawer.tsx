import React from 'react';
import {View, Text, Image, ImageSourcePropType, TouchableOpacity} from "react-native";
import { styles } from "./CustomDrawerStyle";
import {DrawerContentScrollView, DrawerItemList, DrawerContentComponentProps} from "@react-navigation/drawer";
import {useDispatch, useSelector} from "react-redux";
import {selectAuth} from "../../services/store/slices/authSlice";
import {assets, COLORS} from "../../config"
import {Feather, Ionicons} from "@expo/vector-icons";
import {logoutThunk} from "../../services/store/thunks";

const CustomDrawer:React.FC<DrawerContentComponentProps> = (props) => {
    const {user} = useSelector(selectAuth)
    const dispatch = useDispatch()
    const avatar = assets.user as unknown as ImageSourcePropType
    const logout = () => {
        dispatch(logoutThunk())
    }
    //const photo = user.photo ? {uri: user.photo} : avatar
    return (
        <View style={styles.container}>
            <DrawerContentScrollView {...props} contentContainerStyle={styles.contentContainer}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={styles.avatarContainer}>
                            <Image style={styles.userAvatar} source={avatar} />
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <View style={styles.userInfo}>
                                <Text style={styles.title}>
                                    {user?.Firstname} {user?.lastname}
                                </Text>
                                <Text style={styles.caption}>
                                    260 POZOROVANIE
                                </Text>
                            </View>
                            <View style={styles.arrowForward}>
                                <Ionicons name={'md-arrow-forward'} size={20} color={COLORS.WHITE}/>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.drawerItemList}>
                    <DrawerItemList {...props} />
                </View>
            </DrawerContentScrollView>
            <View style={styles.bottomDrawerSection}>
                <TouchableOpacity onPress={() => {}} style={{paddingVertical: 10}}>
                    <View style={styles.bottomDrawerSectionButton}>
                        <Feather name={'settings'} size={20} color={COLORS.grey["500"]}/>
                        <Text style={styles.bottomDrawerSectionButtonText}>
                            Settings
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {logout()}} style={{paddingVertical: 10}}>
                    <View style={styles.bottomDrawerSectionButton}>
                        <Feather name={'log-out'} size={22} color={COLORS.grey["500"]}/>
                        <Text style={styles.bottomDrawerSectionButtonText}>
                            Logout
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default CustomDrawer;