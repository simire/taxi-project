import { StyleSheet } from "react-native";
import {COLORS} from "../../config";

 export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        backgroundColor: COLORS["fuel-yellow"]["600"],
    },
    drawerContent: {
        flex: 1,
        backgroundColor: COLORS["fuel-yellow"]["600"],
    },
    userInfoSection: {
        padding: 20,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    avatarContainer: {
        zIndex: 2,
        elevation: 5,
    },
    userAvatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
     userInfo: {
        flexDirection: "column",
        justifyContent: "space-between",
        marginLeft: 15,
        marginRight: 15,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: COLORS.WHITE,
        marginBottom: 10,
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
        color: COLORS.WHITE
    },
     arrowForward: {
        alignItems: "flex-end",
         justifyContent: "flex-end",
         marginLeft: 15
    },
     drawerItemList: {
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: COLORS.WHITE,
        //borderBottomWidth: 1,
        //borderBottomColor: COLORS.grey["300"],
    },
     bottomDrawerSection: {
        padding: 20,
        backgroundColor: COLORS.WHITE,
        borderTopWidth: 1,
        borderTopColor: COLORS.grey["300"],
    },
     bottomDrawerSectionButton: {
        flexDirection: "row",
        alignItems: "center",
    },
     bottomDrawerSectionButtonText: {
        fontSize: 14,
        color: COLORS.grey["500"],
         fontWeight: "700",
        textAlign: "center",
        marginLeft: 5,
    },
})