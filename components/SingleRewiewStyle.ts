import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 10,
        backgroundColor: "white",
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
    },
    reviewHeader: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
    },
    userImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    userInfoAndReview: {
        flex: 1,
        justifyContent: "center",
        paddingLeft: 10,
        // backgroundColor: "gray"
    },
    review: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        // backgroundColor: 'yellow',
    },
    userName: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#000",
    },
    stars: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    star: {
        width: 20,
        height: 20
    },
    reviewScore: {

    },
    reviewDateContainer: {
        flex: 1,
        alignItems: "flex-end",
        // backgroundColor: "pink",
    },
    reviewDate: {
        fontSize: 12,
        color: "#ccc",
        fontWeight: "bold",
    },
    description: {

    },
    deleteButton: {
        position: 'absolute',
        right: 5,
        top: 5,
        backgroundColor: 'red',
        padding: 5,
        borderRadius: 20
    }
})