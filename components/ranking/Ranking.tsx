import { Stack} from 'native-base';
import React, { useState } from 'react'
import { StyleSheet, Text, View, Platform, ScrollView } from 'react-native';
import { Rating, TapRatingProps } from 'react-native-elements';
import { useReviewsQuery } from '../../services/api/reviewApi';
import { Image } from 'react-native-elements';

type RatingsComponentProps = {};


const Ratings: React.FunctionComponent<RatingsComponentProps> = () => {
  const ratingCompleted = (rating: number) => {
    console.log('Rating is: ' + rating);
  };

  const { data, isSuccess} = useReviewsQuery();
console.log(data);

  const ratingProps = {};
  return (
    <View style={styles.container}>
      <ScrollView style={styles.viewContainer}>
        <Text style={{marginTop: 30, textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>Rating</Text>
        { isSuccess && data.map((file) => ( 
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 20,
            marginBottom: 10,
            borderColor: 'gray',
            borderWidth: 1,
            padding: 10
          }}
          key={file.id} 
        >
           <Image
              source={{ uri:file.fromUser.photoProfile }}
              style={{width: 50, height: 50, marginBottom: 5}}
            />
         
          <Text>{file.fromUser.firstname} {file.fromUser.lastname}</Text>
          <Rating
            showRating
            type="star"
            fractions={1}
            startingValue={file.star}
            imageSize={20}
            onFinishRating={ratingCompleted}
            style={{ paddingVertical: 10 }}
          />
          <Text>{file.review}</Text>
          <Text>{file.route}</Text>
          <Text>{file.toUser.firstname} {file.toUser.lastname}</Text>
          <Image
              source={{ uri:file.toUser.photoProfile }}
              style={{width: 50, height: 50, marginTop: 10}}
            />
        </View>
      
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headingContainer: {
    paddingTop: 50,
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 5,
    fontFamily: Platform.OS === 'ios' ? 'Menlo-Bold' : '',
    color: '#27ae60',
  },
  subtitleText: {
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    fontFamily: Platform.OS === 'ios' ? 'Trebuchet MS' : '',
    color: '#34495e',
  },
  viewContainer: {
    flex: 1,
    marginTop: 30
  },
  rating: {
    paddingVertical: 12,
  },
});

export default Ratings;
