export default interface ReportModel {
    id: number
    name: string;
    title: string;
    date: Date;
    message: string;
  }