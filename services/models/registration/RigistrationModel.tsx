export default interface RegisterModel {
    id: number
    fname: string;
    lname: string;
    city_of_resident: string;
    password: string;
    email: string;
    date_of_birth: string;
    upload_file: {};
    terms_and_condition: string;
    vat: string;
    recipient_code: string;
    taxi_account: string;
    rental_account: string;
  }