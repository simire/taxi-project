import {UserModel} from "./UserModel";

export interface UserWithTokenModel {
  user: UserModel
  token: string
}