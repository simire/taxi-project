export interface UserModel {
    id: number
    firstname: string
    email?: string
    lastname: string
    photoProfile: string
    roles: string[]
}