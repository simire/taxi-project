import {Photo} from "./Photo";
import {UserModel} from "../";
import { ReactNode } from "react";

export interface ReviewModel {
    id: number
    route: string
    star: number
    photoReview: Photo[]
    lang_cod: string
    review: string
    fromUser: UserModel
    toUser: UserModel
}