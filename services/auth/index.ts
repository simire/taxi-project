import {getToken} from "./getToken";
import {setToken} from "./setToken";
import {setUserData} from "./setUserData";
import {getUserData} from "./getUserData";

export {
    getToken,
    getUserData,
    setToken,
    setUserData,
}