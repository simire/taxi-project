import * as SecureStore from 'expo-secure-store';
import {UserWithTokenModel} from "../models";

export const getUserData = async () => {
    const userData = await SecureStore.getItemAsync('userData');
    return userData ? JSON.parse(userData) : null;
};