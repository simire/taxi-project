import * as SecureStore from 'expo-secure-store';

export const setUserData = async (userData: Object | null) => {
    await SecureStore.setItemAsync('userData', JSON.stringify(userData));
};