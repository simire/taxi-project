import * as SecureStore from 'expo-secure-store';

export const getToken = async () => {
    return SecureStore.getItemAsync('token');
};