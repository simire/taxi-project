import {setToken} from "./setToken";
import {LoginModel} from "../models";
import axios from "axios";
import {setUserData} from "./setUserData";

export const authService = {
  login: async (userData: LoginModel) => {
    // const response = await axios.post('https://taxiadvisor.kritekdevelop.net/api/login', userData);
    // Remove dummy data after api fix
    const response = {
      data: {
        Firstname: 'Ervin',
        lastname: 'Hamill',
        token: 'dummy Token',
        id: 51
      }
    }
    if (response.data) {
      await setToken(response.data.token);
      await setUserData(response.data)
    }

    return response.data;
  },
  logout: async () => {
    await setToken('');
    await setUserData(null);
  },
}