import {createAsyncThunk} from "@reduxjs/toolkit";
import {authService} from "../../auth/authService";
import {LoginModel} from "../../models";

export const loginThunk = createAsyncThunk(
    'auth/login',
    async (user:LoginModel, thunkAPI) => {
      try {
        return await authService.login(user);
      } catch (error) {
        const message =  (error.response && error.response.data  && error.response.data.message) || error.message || 'Something went wrong try again';
        return thunkAPI.rejectWithValue(message);
      }
    }
);