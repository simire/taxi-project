import {createAsyncThunk} from "@reduxjs/toolkit";
import {authService} from "../../auth/authService";
import {LoginModel} from "../../models";

export const logoutThunk = createAsyncThunk(
    'auth/logout',
    async (user:undefined, thunkAPI) => {
        try {
            return await authService.logout();
        } catch (e) {
            const message =  'Error while logout';
            return thunkAPI.rejectWithValue(message);
        }
    }
)