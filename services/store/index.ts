import {configureStore} from "@reduxjs/toolkit";
import {reviewApi, usersApi} from "../api";
import authReducer from "./slices/authSlice";
import formLoginReducer from "./slices/formLoginSlice";
import { taxiApi } from "../api/taxiApi";

export const store = configureStore({
    reducer: {
        [reviewApi.reducerPath]: reviewApi.reducer,
        [usersApi.reducerPath]: usersApi.reducer,
        [taxiApi.reducerPath]: taxiApi.reducer,
        auth: authReducer,
        formLogin: formLoginReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(reviewApi.middleware, usersApi.middleware, taxiApi.middleware),
})
