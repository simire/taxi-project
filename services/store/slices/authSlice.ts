import {createSlice, Slice} from "@reduxjs/toolkit";
import {loginThunk, logoutThunk} from "../thunks";

const initialState = {
    isAuthenticated: false,
    isLoading: false,
    user: null,
    isError: false,
    message: ''
};

export const authSlice: Slice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        reset: (state) => {
            state.isAuthenticated = false;
            state.isLoading = false;
            state.isError = false;
        },

    },
    extraReducers: (builder) => {
        builder.addCase(loginThunk.pending, (state) => {
            state.isLoading = true;
        });
        builder.addCase(loginThunk.fulfilled, (state, action) => {
            state.isLoading = false;
            state.isAuthenticated = true;
            state.user = action.payload;
        });
        builder.addCase(loginThunk.rejected, (state, action) => {
            console.log(action.payload)
            state.isLoading = false;
            state.isError = true;
            state.message = String(action.payload);
            state.user = null;
        });
        builder.addCase(logoutThunk.pending, (state) => {
            state.isLoading = true;
        });
        builder.addCase(logoutThunk.fulfilled, (state) => {
            state.isLoading = false;
            state.isAuthenticated = false;
            state.user = null;
        });
        builder.addCase(logoutThunk.rejected, (state, action) => {
            state.isLoading = false;
            state.isError = true;
            state.message = String(action.payload);
        });
    }
});

export const { reset } = authSlice.actions;
export const selectAuth = (state: any) => state.auth;
export default authSlice.reducer;