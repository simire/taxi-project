import {createSlice} from "@reduxjs/toolkit";

const formLoginSlice = createSlice({
    name: 'formLogin',
    initialState: {
        email: '',
        password: '',
        emailError: '',
        passwordError: '',
    },
    reducers: {
        setEmail: (state, action) => {
            state.email = action.payload;
        },
        setPassword: (state, action) => {
            state.password = action.payload;
        },
        setEmailError: (state, action) => {
            state.emailError = action.payload;
        },
        setPasswordError: (state, action) => {
            state.passwordError = action.payload;
        },
    },
});

export const {
    setEmail,
    setPassword,
    setEmailError,
    setPasswordError,
} = formLoginSlice.actions;
export const selectFormLogin = (state: any) => state.formLogin;
export default formLoginSlice.reducer;