import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {UserModel} from "../models";

export const usersApi = createApi({
    reducerPath: 'usersApi',
    baseQuery: fetchBaseQuery({baseUrl: 'https://taxiadvisor.kritekdevelop.net/api/users'}),
    tagTypes: ['UserModel'],
    endpoints: (builder) => ({
        getUsers: builder.query<UserModel[], void>({
            query: () => ({
                url: '/',
                method: 'GET'
            }),
           providesTags: ['UserModel']
        }),
    })
});


export const {useGetUsersQuery} = usersApi;
