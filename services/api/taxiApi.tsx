import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import RegisterModel from '../models/registration/RigistrationModel';
import ReportModel from '../models/report/reportModel';

export const taxiApi = createApi({

  reducerPath: 'postsApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://taxiadvisor.kritekdevelop.net/api'}),
refetchOnFocus: true,
tagTypes: ['Posts'],
  endpoints: (builder) => ({
    getPosts: builder.query<RegisterModel[], void>({
      query: ()  => '/',
        providesTags: (result) => result ? [...result.map(({ id }) => ({ type: 'Posts' as const, id })),
                { type: 'Posts', id: 'LIST' },
              ] : [{ type: 'Posts', id: 'LIST' }],
    }),
       getPost: builder.query<RegisterModel, any>({
          query: (id) => `/${id}`,
          providesTags: (result, error, id: any) =>  [{ type:'Posts', id }],  
        }),

     addPost: builder.mutation<RegisterModel, Partial<RegisterModel>>({
      query(body) {
        return {
        url: '/',
        method: 'POST',
        body,
      }
       },
        invalidatesTags: [{type: 'Posts', id: 'LIST'}],
     }),

     updatePost: builder.mutation<RegisterModel, Partial<RegisterModel>>({
        query(data) {
          const { id, ...body} = data;
          return {
          url: `/${id}`,
          method: 'PATCH',
          body,  
          }
         },
         async onQueryStarted({ id, ...patch }, { dispatch, queryFulfilled }) {
          const patchResult = dispatch(
            taxiApi.util.updateQueryData('getPost', id, (draft) => {
              Object.assign(draft, patch)
            })
          )
          try {
            await queryFulfilled
          } catch {
            patchResult.undo()
          }
        },
         invalidatesTags:  (result, error, {id}) => [{ type: 'Posts', id: 'LIST'  }],
       }),

       deletePost: builder.mutation<{ success: boolean; id: number }, number>({
        query(id) {
          return {
            url: `posts/${id}`,
            method: 'DELETE',
          }
        },
       invalidatesTags: (result, error, id) => [{ type: 'Posts', id: 'LIST' }],
         }),

         registration: builder.mutation<RegisterModel, Partial<RegisterModel>>({
          query(body) {
            return {
            url: '/register',
            method: 'POST',
            body,
          }
           },
            invalidatesTags: [{type: 'Posts', id: 'LIST'}],
         }),
         getReports: builder.query<ReportModel[], void>({
          query: ()  => '/reports',
            providesTags: (result) => result ? [...result.map(({ id }) => ({ type: 'Posts' as const, id })),
                    { type: 'Posts', id: 'LIST' },
                  ] : [{ type: 'Posts', id: 'LIST' }],
        }),
           getReport: builder.query<ReportModel, any>({
              query: (id) => `reports/${id}`,
              providesTags: (result, error, id: any) =>  [{ type:'Posts', id }],  
            }),
    
         
         addReport: builder.mutation<ReportModel, Partial<ReportModel>>({
          query(body) {
            return {
            url: '/reports',
            method: 'POST',
            body,
          }
           },
            invalidatesTags: [{type: 'Posts', id: 'LIST'}],
         }),
         
         updateReport: builder.mutation<ReportModel, Partial<ReportModel>>({
          query(data) {
            const { id, ...body} = data;
            return {
            url: `reports/${id}`,
            method: 'PATCH',
            body,  
            }
           },
           async onQueryStarted({ id, ...patch }, { dispatch, queryFulfilled }) {
            const patchResult = dispatch(
              taxiApi.util.updateQueryData('getReport', id, (draft) => {
                Object.assign(draft, patch)
              })
            )
            try {
              await queryFulfilled
            } catch {
              patchResult.undo()
            }
          },
           invalidatesTags:  (result, error, {id}) => [{ type: 'Posts', id: 'LIST'  }],
         }),
  
         deleteReport: builder.mutation<{ success: boolean; id: number }, number>({
          query(id) {
            return {
              url: `reports/${id}`,
              method: 'DELETE',
            }
          },
         invalidatesTags: (result, error, id) => [{ type: 'Posts', id: 'LIST' }],
           }),
       
       }),
     
})

export const { 
    useGetPostsQuery,
    useGetPostQuery, 
    useAddPostMutation, 
    useUpdatePostMutation,
    useDeletePostMutation,
    useRegistrationMutation, 
    useGetReportsQuery,
    useGetReportQuery,
    useAddReportMutation, 
    useUpdateReportMutation,
    useDeleteReportMutation,
 } = taxiApi