import {reviewApi} from "./reviewApi";
import {usersApi} from "./usersApi";

export {
    reviewApi,
    usersApi
}
