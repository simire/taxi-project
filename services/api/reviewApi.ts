import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {ReviewModel} from "../models";

export const reviewApi = createApi({
    reducerPath: 'reviewApi',
    baseQuery: fetchBaseQuery({baseUrl: 'https://taxiadvisor.kritekdevelop.net/api/reviews'}),
    tagTypes: ['ReviewModel'],
    endpoints: (builder) => ({
        reviews: builder.query<ReviewModel[], void>({
            query: () => ({
                method: 'GET',
                url: '/',
            }),
            providesTags: ['ReviewModel'],
        }),
        addReview: builder.mutation({
            query: (review) => ({
                method: 'POST',
                url: '',
                body: review
            }),
            invalidatesTags: ['ReviewModel']
        }),
        deleteReview: builder.mutation({
            query: (id) => ({
                method: 'DELETE',
                url: `/${id}`,
                credentials: 'include'
            }),
            invalidatesTags: ['ReviewModel']
        }),
        latestReviews: builder.query<ReviewModel[], void>({
            query: () => ({
                method: 'GET',
                url: '/latest'
            }),
            providesTags: ['ReviewModel'],
        })
    }),
})

export const {useReviewsQuery, useAddReviewMutation, useDeleteReviewMutation, useLatestReviewsQuery} = reviewApi;
