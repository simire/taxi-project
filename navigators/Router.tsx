import React, {useEffect} from 'react';
import {NavigationContainer} from "@react-navigation/native";
import AppNavigator from "./AppNavigator";
import AuthNavigator from "./AuthNavigator";
import {useDispatch, useSelector} from "react-redux";
import {reset, selectAuth} from "../services/store/slices/authSlice";
import { Loading } from "../components";
import { getUserData } from "../services/auth";
import {loginThunk} from "../services/store/thunks";

const Router = () => {
    const {isAuthenticated, isLoading, user} = useSelector(selectAuth)
    const dispatch = useDispatch();
    useEffect(() => {
        getUserData().then(userData => {
            if (userData) {
                dispatch(loginThunk({email: userData.email, password: userData.password}))
            }
        })
        dispatch(reset)
    }, [dispatch])

    if (isLoading) {
        return <Loading />
    }
    return (
        <NavigationContainer>
            {(user?.token || isAuthenticated) ? <AppNavigator/> : <AuthNavigator/>}
        </NavigationContainer>
    );
};

export default Router;