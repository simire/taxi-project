import React from 'react';
import {ItemScreen, ReviewScreen} from "../screens";
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import {createDrawerNavigator} from "@react-navigation/drawer";
import {CustomDrawer} from "../components/CustomDrawer";
import {Feather} from "@expo/vector-icons";
import {COLORS} from "../config";
import {View} from "react-native";
import AddReviewScreen from "../screens/Review/AddReviewScreen";


const Drawer = createDrawerNavigator();

const AppNavigator = () => {
    return (
        <SafeAreaProvider>
            <Drawer.Navigator
                initialRouteName="Reviews"
                drawerContent={props => <CustomDrawer {...props} />}
                screenOptions={({route, navigation}) => ({
                    headerTitle: route.name,
                    headerLeft: () => (
                        <Feather
                            name="menu"
                            color={COLORS.BLACK}
                            size={25}
                            style={{marginLeft: 10}}
                            onPress={() => {
                                navigation.toggleDrawer();
                            }}
                        />
                    ),
                    headerRight: () => (
                        <View style={{marginRight: 10, flexDirection: 'row'}}>
                            <Feather
                                name={'list'}
                                size={25}
                                color={COLORS.BLACK}
                                style={{marginRight: 10}}
                            />
                            <Feather
                                name="search"
                                color={COLORS.BLACK}
                                size={25}
                                style={{marginRight: 10}}
                            />
                            <Feather
                                name="more-vertical"
                                color={COLORS.BLACK}
                                size={25}
                                style={{marginRight: 10}}
                            />
                        </View>
                    ),
                    drawerActiveBackgroundColor: COLORS["fuel-yellow"]["600"],
                    drawerActiveTintColor: COLORS.WHITE,
                    drawerInactiveTintColor: COLORS.grey["500"],
                    drawerLabelStyle: {
                        marginLeft: -25,
                        fontSize: 15,
                        fontWeight: '700',
                    }
                })}
            >
                <Drawer.Screen
                    name="Reviews"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'star'} size={22} color={color} />
                    }}
                    component={ReviewScreen}
                />
                <Drawer.Screen
                    name="AddReview"
                    component={AddReviewScreen}
                    options={{
                        drawerIcon: ({color}) => <Feather name={'plus'} size={22} color={color} />
                    }}
                />
                <Drawer.Screen
                    name="Item1"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'code'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item2"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'codepen'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item3"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'coffee'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item4"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'calendar'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item5"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'tag'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item6"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'figma'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
                <Drawer.Screen
                    name="Item7"
                    options={{
                        drawerIcon: ({color}) => <Feather name={'github'} size={22} color={color} />
                    }}
                    component={ItemScreen}
                />
            </Drawer.Navigator>
        </SafeAreaProvider>
    );
};

export default AppNavigator;
