import React from 'react';
import { Text, View } from 'react-native'
import {LoginScreen, TermsCondition} from "../screens";
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();


const AuthNavigator = () => {
    return (
        <SafeAreaProvider>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Login" component={LoginScreen}/>
                <Stack.Screen name="Termscondition" component={TermsCondition} />
            </Stack.Navigator>
        </SafeAreaProvider>
    );
};

export default AuthNavigator;