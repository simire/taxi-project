import star from '../assets/images/star.png'
import user from '../assets/images/user.png'
import logo from '../assets/images/logo.jpg'

export default {
    star,
    user,
    logo
}