import {StyleSheet} from "react-native";
import {COLORS} from "../../config";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS["fuel-yellow"].DEFAULT
    },
    logoContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 100,
        height: 100,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 45,
        paddingTop: 45,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.WHITE,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        elevation: 5,
        shadowColor: COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        zIndex: 5
    },
    inputContainer: {
        width: '100%',
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ff0000'
    },
    input: {
        marginTop: 10,
        marginBottom: 2,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        borderBottomWidth: 1,
        textDecorationLine: 'none',
        width: '100%',
        padding: 5,
    },
    error: {
        color: 'red',
        fontSize: 12,
        marginBottom: 5,
    },
    button: {
        width: '100%',
        padding: 10,
        backgroundColor: '#e7b020',
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        textTransform: 'uppercase',
        textDecorationLine: 'none',
    },
    termsTxt:{
        fontSize: 18,
        color : 'gray'
    }
})

export default styles