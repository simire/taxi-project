import React from 'react';
import {Image, ImageSourcePropType, Text, View, TouchableOpacity} from "react-native";
import {assets, COLORS} from "../../config";
import styles from "./LoginScreenStyle"
import {CustomButton, InputText, LoginForm} from "../../components/form";
import {loginThunk} from "../../services/store/thunks";
import {useDispatch, useSelector} from "react-redux";
import {
    selectFormLogin,
    setEmail,
    setEmailError,
    setPassword,
    setPasswordError
} from "../../services/store/slices/formLoginSlice";

const LoginScreen = ({navigation}: any) => {
    const dispatch = useDispatch();
    const {email, password, emailError, passwordError} = useSelector(selectFormLogin)
    const [emailBorderBottomColor, setEmailBorderBottomColor] = React.useState("#ccc");
    const [passwordBorderBottomColor, setPasswordBorderBottomColor] = React.useState("#ccc");

    const onEmailChange = (text: string) => {
        dispatch(setEmail(text))
    };

    const onPasswordChange = (text: string) => {
        dispatch(setPassword(text))
    };

    const handleSubmit = async () => {
        if (email.length === 0) {
            dispatch(setEmailError("Email is required"))
            setEmailBorderBottomColor("#f00");
        } else if (email.length < 5) {
            dispatch(setEmailError("Email is too short"))
            setEmailBorderBottomColor("#f00");
        }  else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            dispatch(setEmailError("Email is invalid"))
            setEmailBorderBottomColor("#f00");
        } else if (password.length === 0) {
            dispatch(setPasswordError("Password is required"))
            setPasswordBorderBottomColor("#f00");
        } else if (password.length < 5) {
            dispatch(setPasswordError("Password is too short"))
            setPasswordBorderBottomColor("#f00");
        } else {
            dispatch(setEmailError(""))
            dispatch(setPasswordError(""))
            dispatch(setEmail(""))
            dispatch(setPassword(""))
            setEmailBorderBottomColor("#4CAF50");
            setPasswordBorderBottomColor("#4CAF50");
            console.log("try to login");
            dispatch(loginThunk({email, password}));
        }
    };

    const Logo = assets.logo as unknown as ImageSourcePropType
    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <View style={{backgroundColor: '#fff', width: 120, alignItems: 'center', justifyContent: 'center', height: 120, padding: 20, borderRadius: 120/2, elevation: 2}}>
                    <Image
                        source={Logo}
                        resizeMode={'contain'}
                        style={styles.logo}
                    />
                </View>
            </View>
            <LoginForm style={styles.formContainer}>
                <InputText
                    placeholder={'Email'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    style={[styles.input, {borderBottomColor: emailBorderBottomColor}]}
                    onFocus={() => setEmailBorderBottomColor(COLORS["fuel-yellow"].DEFAULT)}
                    onBlur={() => setEmailBorderBottomColor(COLORS.grey["300"])}
                    container={styles.inputContainer}
                    keyboardType={'email-address'}
                    value={email}
                    onChangeText={onEmailChange}
                />
                <Text style={styles.error}>{emailError}</Text>
                <InputText
                    placeholder={'Password'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    style={[styles.input, {borderBottomColor: passwordBorderBottomColor}]}
                    onFocus={() => setPasswordBorderBottomColor(COLORS["fuel-yellow"].DEFAULT)}
                    onBlur={() => setPasswordBorderBottomColor(COLORS.grey["300"])}
                    container={styles.inputContainer}
                    secureTextEntry={true}
                    value={password}
                    onChangeText={onPasswordChange}
                />
                <Text style={styles.error}>{passwordError}</Text>
                <CustomButton
                    onPress={handleSubmit}
                    text={'Entrar'}
                    style={styles.button}
                    textStyle={styles.buttonText}
                />
            
            <TouchableOpacity onPress={()=> navigation.navigate('Termscondition')}>
                <Text style={styles.termsTxt}>View terms & conditions</Text>
            </TouchableOpacity>

            </LoginForm>
         
        </View>
    );
};

export default LoginScreen;