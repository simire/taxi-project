import { StyleSheet } from "react-native";
import { COLORS } from "../../config";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 20
    },
    button: {
        backgroundColor: "#FFFFFF",
        width: "100%",
        padding: 10,
        marginBottom: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        overflow: "hidden",
        elevation: 5
    },
    buttonText: {
        color: "#00BFFF",
        fontSize: 20,
        textAlign: "center",
        fontWeight: "bold",
    },
    latestReviews: {
        backgroundColor: 'white',
        padding: 5,
        borderRadius: 10,
        borderColor: 'transparent',
        borderWidth: 2
    }
});