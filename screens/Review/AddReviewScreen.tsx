import React, {useEffect, useState} from 'react';
import {View, Text, TextInput, Pressable} from 'react-native';
import {useSelector} from 'react-redux';
import {useGetUsersQuery} from '../../services/api/usersApi';
import {useAddReviewMutation} from '../../services/api/reviewApi';

import {Picker} from '@react-native-picker/picker';

import { selectAuth} from '../../services/store/slices/authSlice';
import {useNavigation} from "@react-navigation/core";
import {UserModel} from "../../services/models";
import styles from './AddReviewStyle'
import {COLORS} from "../../config";
import {Feather} from "@expo/vector-icons";

const AddReviewScreen = () => {

    const {user} = useSelector(selectAuth);
    const navigation = useNavigation();
    type pickerDataType = {
        label: string,
        value: number
    }[]

    const {data, isLoading} = useGetUsersQuery();
    const [adder] = useAddReviewMutation();
    const [pickerData, setPickerData] = useState<pickerDataType | undefined>([]);
    const [review, setReview] = useState('');
    const [LangCod, setLangCod] = useState('');
    const [startNumber, setStartNumber] = useState('');
    const [route, setRoute] = useState('');
    const [userId, setUserId] = useState('');

    useEffect(() => {
        if (!isLoading) {
            let picker = data?.map(item => ({label: item.firstname, value: item.id}));
            setPickerData(picker)
        }
    }, [isLoading])

    const onSubmit = async (reviewData: { route: string; lang_cod: string; review: string; star: number; toUser: UserModel | undefined; fromUser: any; }) => {
        try {
            await adder(reviewData);
            navigation.goBack();
            reset();
        } catch (e) {
            console.log(e.message)
        }
    };


    const reset = () => {
        setUserId('');
        setReview('');
        setRoute('');
        setStartNumber('');
        setLangCod('');
    }

    const PickerItems = () => {
        return pickerData?.map( ({label, value}) => (
            <Picker.Item key={`${label}-${value}`} label={`${label}`} value={`${value}`} />
            )
        )
    }

    return (
        <View style={styles.container}>
            <Text>Select a user to review </Text>
            <Picker
                selectedValue={userId}
                onValueChange={(itemValue) => {
                    setUserId(itemValue);
                }}
            >
                {PickerItems()}
            </Picker>
            <TextInput
                multiline={true}
                value={review}
                onChangeText={(reviewText) => setReview(reviewText)}
                placeholder='Write a review here'
                numberOfLines={5}
                style={styles.textareaInput}
            />
            <TextInput
                value={`${startNumber}`}
                onChangeText={(stars) => setStartNumber(stars)}
                placeholder='Start number'
                style={styles.textInput}
                keyboardType={'numeric'}
            />
            <TextInput
                value={`${LangCod}`}
                onChangeText={(langCod) => setLangCod(langCod)}
                placeholder='LangCod'
                keyboardType={'numeric'}
                style={styles.textInput}
            />
            <TextInput
                value={`${route}`}
                onChangeText={(r) => setRoute(r)}
                placeholder='route'
                style={styles.textInput}
            />
            <View style={styles.buttonContainer}>
                <Pressable
                    style={[styles.button, styles.cancelButton]}
                    onPress={() => navigation.goBack()}
                >
                    <Feather
                        name="x"
                        color={COLORS.WHITE}
                        size={20}
                        style={{marginRight: 10}}
                    />
                    <Text style={styles.buttonText}>Cancel</Text>
                </Pressable>
                <Pressable
                    style={[styles.button, styles.submitButton]}
                    onPress={() => onSubmit({route, lang_cod: LangCod, review, star: parseInt(startNumber), toUser: data?.filter(item => item.id as unknown as string == userId)[0], fromUser:user})}
                >
                    <Feather
                        name="save"
                        color={COLORS.WHITE}
                        size={20}
                        style={{marginRight: 10}}
                    />
                    <Text style={styles.buttonText}>Submit</Text>
                </Pressable>
            </View>
        </View>
    );
};

export default AddReviewScreen;
