import {FlatList, Text, TouchableOpacity, View} from 'react-native'
import React, { useState } from 'react'
import {styles} from './ReviewStyle'
import {Loading, SingleReview} from '../../components'
import {SafeAreaView} from "react-native-safe-area-context";
import {useLatestReviewsQuery, useReviewsQuery} from "../../services/api/reviewApi";
import {selectAuth} from "../../services/store/slices/authSlice";
import {useSelector} from "react-redux";
import {useNavigation} from "@react-navigation/core"
import { COLORS } from '../../config';



const ReviewScreen = () => {
    const [isLatest, setIsLatest] = useState(false);

    const onClick = () => {
        setIsLatest(oldIsLatest => !oldIsLatest);
    };
    

    const {data: reviews, isLoading, isError, error} = useReviewsQuery();
    const {data: latestReviews, isLoading: isLatestLoading, isError: isLatestError, error: latestError} = useLatestReviewsQuery();
    const {isLoading: isLoggingOut} = useSelector(selectAuth)
    const navigation = useNavigation()

    if (isLoading || isLatestLoading || isLoggingOut) {
        return <Loading />
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity style={[styles.latestReviews, isLatest && {borderColor: COLORS['fuel-yellow'][600]}]} onPress={onClick} >
                <Text>Latest Reviews</Text>
            </TouchableOpacity>
            <View>
                <FlatList
                    data={isLatest? latestReviews : reviews}
                    renderItem={({item}) => <SingleReview data={item} />}
                    keyExtractor={item => item.id.toString()}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('AddReview' as unknown as never)}>
                <View>
                    <Text style={styles.buttonText}>Write Review</Text>
                </View>
            </TouchableOpacity>

        </SafeAreaView>
    )
}

export default ReviewScreen
