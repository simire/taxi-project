import React, { useState } from 'react';
import {ScrollView, Input, Stack, Button, Radio, Flex,  FormControl, IconButton,} from "native-base";
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import {useRegistrationMutation} from '../services/api/taxiApi';
import { useForm, Controller } from "react-hook-form";
import * as DocumentPicker from 'expo-document-picker';
//import * as FileSystem from 'expo-file-system'
import { StatusBar } from 'expo-status-bar';



function RegistrationForm() {

 const [registration] = useRegistrationMutation()
const [register, setRegister] = useState({fname: '', lname: '', city_of_residence: '', email: '', date_of_birth: '', password: '', upload_file: '', vat: '', recipient_code: '', terms_and_condition: 'Accepted', account: ''})

  const [ doc, setDoc ] = useState({});
  const pickDocument = async () => {
      let result = await DocumentPicker.getDocumentAsync({ type: "*/*", copyToCacheDirectory: true }).then(response => {
          if (response.type == 'success') {          
            let { name, size, uri } = response;
            let nameParts = name.split('.');
            let fileType = nameParts[nameParts.length - 1];
            let fileToUpload = {
              name: name,
              size: size,
              uri: uri,
              type: "application/" + fileType
            };
           // console.log(fileToUpload, 'file')
            setDoc(fileToUpload);
          } 
        });
      //console.log(result);
     // console.log("Doc: " + doc.uri);
  }

  const postDocument = () => {
    const url = "https://taxiadvisor.kritekdevelop.net/api/register";
   const fileUri = doc.uri;
    const formData = new FormData();
    formData.append('document', doc);
    const options = {
        method: 'POST',
        body: formData,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
    };
   console.log(doc.uri);

    fetch(url, options).catch((error) => console.log(error));
}


const { control, handleSubmit, formState: { errors } } = useForm({
  defaultValues: {
    fname: '', lname: '', city_of_residence: '', email: '', date_of_birth: '', password: '', upload_file: doc.uri, vat: '', recipient_code: '', terms_and_condition: 'Accepted', account: '' 
  }
});

const onSubmit = (data: any) => {
  registration({...register})
};


const [pay, setPay] = useState({value: ''})
 console.log(register);

  return (
    <ScrollView>
      
      <Stack space={1} w="100%" maxW="360px" mx="auto" mt={10}>
      <Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginBottom: 10}}>REGISTER</Text>
        <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
         <Radio.Group defaultValue="Taxi account" name="myRadioGroup" accessibilityLabel="Pick your favorite number" onChange={(account: any) =>   setRegister({...register, account})} value={register.account}>
         <Flex direction='row' ml='1'>
     <Radio  style={{marginRight: 10}} size="sm" value='Taxi account'>Taxi Account</Radio>
     <Radio size="sm" value='Rental account' >Rental Account</Radio>
     </Flex>
     </Radio.Group>
      )} name="account" rules={{ required: true }} />
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>First name</FormControl.Label>
     <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="First name"  onChangeText={(fname: any) =>   setRegister({...register, fname})} value={register.fname}/>
      )} name="fname" rules={{ required: true}} />
      <FormControl.ErrorMessage >First name is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>Last name</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="Last name" onBlur={onBlur} onChangeText={(lname: any) =>   setRegister({...register, lname})} value={register.lname} />
      )} name="lname" rules={{ required: true }} />
      <FormControl.ErrorMessage >Last name is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>City of residence</FormControl.Label>
       <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="City of residence" onBlur={onBlur} onChangeText={(city_of_residence: any) =>   setRegister({...register, city_of_residence})} value={register.city_of_residence} />
      )} name="city_of_residence" rules={{ required: true }} />
      <FormControl.ErrorMessage>city is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>Email</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="Email" onBlur={onBlur} onChangeText={(email: any) =>   setRegister({...register, email})} value={register.email}  />
      )} name="email" rules={{ required: true }} />
      <FormControl.ErrorMessage>email is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>Password</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='password' placeholder="Password" onBlur={onBlur} onChangeText={(password: any) =>   setRegister({...register, password})} value={register.password} />
      )} name="password" rules={{ required: true }} />
      <FormControl.ErrorMessage>password is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl isRequired w="100%" minW="360px">
        <FormControl.Label>Date of birth</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="Date of birth" onBlur={onBlur} onChangeText={(date_of_birth: any) =>   setRegister({...register, date_of_birth})} value={register.date_of_birth}  />
      )} name="date_of_birth" rules={{ required: true }} />
      <FormControl.ErrorMessage>Date of birth is required</FormControl.ErrorMessage>
      </FormControl>
      <FormControl  w="100%" minW="360px">
        <FormControl.Label>Upload licence or certification</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
    <View>
      <Flex direction='row' ml='1'>
    <Button style={{ marginRight: 10}} onPress={pickDocument}>Select a file</Button>
    <Button onPress={postDocument}>Upload file</Button>
    </Flex>
    </View>
      )} name='upload_file' />
      <FormControl.ErrorMessage>ID is required</FormControl.ErrorMessage>
      </FormControl>
     
      <FormControl w="100%" minW="360px">
        <FormControl.Label>VAT</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="VAT" onBlur={onBlur} onChangeText={(vat: any) =>   setRegister({...register, vat})} value={register.vat} />
      )} name="vat" />
      <FormControl.ErrorMessage></FormControl.ErrorMessage>
      </FormControl>
      <FormControl w="100%" minW="360px">
        <FormControl.Label>Recipient code</FormControl.Label>
      <Controller control={control} render={({field: { onChange, onBlur, value }}) => (
      <Input size="xs" type='text' placeholder="Recipient code" onBlur={onBlur} onChangeText={(recipient_code: any) =>   setRegister({...register, recipient_code})} value={register.recipient_code} />
      )} name="recipient_code"/>
      <FormControl.ErrorMessage></FormControl.ErrorMessage>
      </FormControl>
      <Controller control={control} render={({field: { onChange, value }}) => (
      <Radio.Group defaultValue="Accepted" name="myRadioGroup" accessibilityLabel="Pick your favorite number" onChange={(terms_and_condition: any) =>   setRegister({...register, terms_and_condition})} value={register.terms_and_condition} >
      <Flex direction='row' ml='1'>
     <Radio value='Accepted' size="sm">Accept term of use</Radio>
     </Flex>
     </Radio.Group>
     )} name="terms_and_condition" rules={{ required: true }} />
     { 
      register.account === 'Taxi account' ?
        <Button style={{marginTop: 10}}>Click to pay</Button> : <Text>''</Text>
      
     }
       
     
    
     <Button style={{marginTop: 10}} onPress={onSubmit}>Submit</Button>
    </Stack>
    </ScrollView>
  )
}

export default RegistrationForm

//<Input size="xs" placeholder="Upload licence or certification" onBlur={onBlur} onChangeText={value => onChange(value)} value={value}   />